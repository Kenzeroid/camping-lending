﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Camp.DataModel;
using CampViewModel;

namespace CampRepo
{
    public class ProductRepo
    {
        public static List<Product> All()
        {
            using (var db = new CampContext())
            {
                var ListProduct = db.Products.ToList();
                return ListProduct;
            }
        }
        public static RespondResult Update(Product entity)
        {
            RespondResult result = new RespondResult();
            try
            {
                using (var db = new CampContext())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        Product product = new Product();
                        product.name = entity.name;
                        product.amount = entity.amount;
                        product.initial = entity.initial;
                        product.description = entity.description;
                        product.price = entity.price;
                        product.create_by = "admin";
                        product.create_on = DateTime.Now;
                        product.is_deleted = false;
                        db.Products.Add(product);
                        db.SaveChanges();
                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        Product product = db.Products
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                        if (product != null)
                        {
                            product.name = entity.name;
                            product.amount = entity.amount;
                            product.initial = entity.initial;
                            product.description = entity.description;
                            product.price = entity.price;
                            product.modified_by = "admin";
                            product.modified_on = DateTime.Now;
                            db.SaveChanges();
                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Data tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        public static ProductViewModel ById(long id)
        {
            ProductViewModel result = new ProductViewModel();
            using (var db = new CampContext())
            {
                result = (from c in db.Products
                          where c.is_deleted == false && c.id == id
                          select new ProductViewModel
                          {
                              id = c.id,
                              name = c.name,
                              initial = c.initial,
                              amount = c.amount,
                              price = c.price
                          }).FirstOrDefault();
            }
            return result != null ? result : new ProductViewModel();
        }
        public static void Decrement(long id, long quantity)
        {
            using (var db = new CampContext())
            {
                Product product = db.Products
                        .Where(o => o.id == id)
                        .FirstOrDefault();
                if (product != null)
                {
                    product.amount = product.amount - quantity;
                    product.modified_by = "admin";
                    product.modified_on = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }
        public static void Increment(long id, long quantity)
        {
            using (var db = new CampContext())
            {
                Product product = db.Products
                        .Where(o => o.id == id)
                        .FirstOrDefault();
                if (product != null)
                {
                    product.amount = product.amount + quantity;
                    product.modified_by = "admin";
                    product.modified_on = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }
        public static RespondResult Delete(Product entity)
        {
            RespondResult result = new RespondResult();
            try
            {
                using (var db = new CampContext())
                {

                    Product product = db.Products
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (product != null)
                    {
                        product.is_deleted = true;
                        product.deleted_by = "admin";
                        product.deleted_on = DateTime.Now;
                        db.SaveChanges();
                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Data tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
