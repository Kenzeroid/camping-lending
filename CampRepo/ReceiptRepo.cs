﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Camp.DataModel;
using CampViewModel;

namespace CampRepo
{
    public class ReceiptRepo
    {
        public static List<ReceiptHeadViewModel> receiptHeads()
        {
            List<ReceiptHeadViewModel> receipts = new List<ReceiptHeadViewModel>();
            using (var db = new CampContext())
            {
                receipts = (from c in db.ReceiptHeads
                            join b in db.Biodatas
                            on c.biodataId equals b.id
                            where c.is_deleted == false
                            select new ReceiptHeadViewModel
                            {
                                id = c.id,
                                biodataId = b.id,
                                borrowTime = c.borrowTime,
                                Code = c.Code,
                                status = c.status,
                                total = c.total,
                                create_on = c.create_on
                            }).ToList();
            }
            return receipts;
        }
        public static List<ReceiptHead> All()
        {
            using (var db = new CampContext())
            {
                var ListReceipt = db.ReceiptHeads.ToList();
                return ListReceipt;
            }
        }
        public static RespondResult Update(ReceiptHead entity)
        {
            RespondResult result = new RespondResult();
            try
            {
                using (var db = new CampContext())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        ReceiptHead Receipt = new ReceiptHead();
                        Receipt.create_by = "admin";
                        Receipt.create_on = DateTime.Now;
                        Receipt.is_deleted = false;
                        db.ReceiptHeads.Add(Receipt);
                        db.SaveChanges();
                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        ReceiptHead Receipt = db.ReceiptHeads
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                        if (Receipt != null)
                        {

                            Receipt.modified_by = "admin";
                            Receipt.modified_on = DateTime.Now;
                            db.SaveChanges();
                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Data tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        public static RespondResult Return(long id, int diff)
        {
            RespondResult result = new RespondResult();
            try
            {
                using (var db = new CampContext())
                {

                    ReceiptHead Receipt = db.ReceiptHeads
                    .Where(o => o.id == id)
                    .FirstOrDefault();
                    if (Receipt != null)
                    {
                        Receipt.returnTime = DateTime.Now.ToString("dddd, dd MMMM yyyy");
                        Receipt.status = true;
                        Receipt.modified_by = "admin";
                        Receipt.modified_on = DateTime.Now;
                        int fine = 0;
                        if(diff > 0)
                        {
                            fine = diff * 10000;
                            result.Message = "Pengembalian terlambat "+ diff + " hari, Denda "+fine;
                            Receipt.total = Receipt.total + fine;
                        }
                        else
                        {
                            result.Message = "Terima kasih"; 
                        }
                        List<ReceiptDetail> receipt = db.ReceiptDetails.Where(o => o.receiptHeadId == id).ToList();
                        foreach(var item in receipt)
                        {
                            ProductRepo.Increment(item.productId, item.quantity);
                        }
                        db.SaveChanges();                        
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Data tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        public static RespondResult Payment(ReceiptHeadViewModel entity)
        {
            RespondResult result = new RespondResult();
            try
            {
                using (var db = new CampContext())
                {
                    string theCode = CodeGenerator();
                    ReceiptHead receiptHead = new ReceiptHead();
                    receiptHead.Code = theCode;
                    receiptHead.is_deleted = false;
                    receiptHead.biodataId = entity.biodataId;
                    receiptHead.borrowTime = entity.borrowTime;
                    receiptHead.create_by = "Admin";
                    receiptHead.create_on = DateTime.Now;
                    receiptHead.status = false;
                    receiptHead.total = entity.total;
                    db.ReceiptHeads.Add(receiptHead);
                    db.SaveChanges();
                    foreach (var item in entity.ReceiptDetailViewModel)
                    {
                        ReceiptDetail receiptDetail = new ReceiptDetail();
                        receiptDetail.receiptHeadId = receiptHead.id;
                        receiptDetail.productId = item.productId;
                        ProductRepo.Decrement(item.productId, item.quantity);
                        receiptDetail.price = item.amount;
                        receiptDetail.quantity = item.quantity;
                        receiptDetail.create_by = "Admin";
                        receiptDetail.create_on = DateTime.Now;
                        db.ReceiptDetails.Add(receiptDetail);
                        db.SaveChanges();
                    }
                    result.Entity = entity;
                    result.Message = theCode;
                }
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        public static string CodeGenerator()
        {
            string LastCode = "";
            int digit = 6;
            int digitnol = 0;
            string jmlNol = "";
            string strKode = "";
            using (var db = new CampContext())
            {
                LastCode = db.ReceiptHeads.OrderByDescending(a => a.Code).Select(a => a.Code).FirstOrDefault();
                if (LastCode == null)
                {
                    LastCode = "UD00000";
                }
                else
                {
                    strKode = "UD";
                    int angka = int.Parse(LastCode.Substring(3, 5));
                    angka += 1;
                    if (angka.ToString().Length <= digit)
                    {
                        digitnol = digit - angka.ToString().Length;
                        for (int i = 0; i < digitnol; i++)
                        {
                            jmlNol += "0";
                        }
                    }
                    LastCode = strKode + jmlNol + angka;
                }                
            }
            return LastCode;
        }
        public static List<ReceiptDetailViewModel> Details(long id)
        {
            List<ReceiptDetailViewModel> result = new List<ReceiptDetailViewModel>();
            using (var db = new CampContext())
            {
                result = (from c in db.ReceiptHeads
                          join d in db.ReceiptDetails
                          on c.id equals d.receiptHeadId
                          join e in db.Products
                          on d.productId equals e.id
                          where c.is_deleted == false && c.id == id
                          select new ReceiptDetailViewModel
                          {
                              id = d.id,
                              receiptHeadId = c.id,
                              productId = e.id,
                              quantity = d.quantity,
                              price = d.price
                          }).ToList();
            }
            return result;
        }
    }
}
