﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CampViewModel;
using Camp.DataModel;

namespace CampRepo
{
    public class BiodataRepo
    {
        public static List<Biodata> All()
        {
            using (var db = new CampContext())
            {
                var ListBiodata = db.Biodatas.ToList();
                return ListBiodata;
            }
        }
        public static RespondResult Update(Biodata entity)
        {
            RespondResult result = new RespondResult();
            try
            {
                using (var db = new CampContext())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        Biodata Biodata = new Biodata();
                        Biodata.fullName = entity.fullName;
                        Biodata.pob = entity.pob;
                        Biodata.email = entity.email;
                        Biodata.dob = entity.dob;
                        Biodata.position = entity.position;
                        Biodata.create_by = "admin";
                        Biodata.create_on = DateTime.Now;
                        Biodata.is_deleted = false;
                        db.Biodatas.Add(Biodata);
                        db.SaveChanges();
                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        Biodata Biodata = db.Biodatas
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                        if (Biodata != null)
                        {
                            Biodata.fullName = entity.fullName;
                            Biodata.pob = entity.pob;
                            Biodata.email = entity.email;
                            Biodata.dob = entity.dob;
                            Biodata.position = entity.position;
                            Biodata.modified_by = "admin";
                            Biodata.modified_on = DateTime.Now;
                            db.SaveChanges();
                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Data tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        public static RespondResult Delete(Biodata entity)
        {
            RespondResult result = new RespondResult();
            try
            {
                using (var db = new CampContext())
                {

                    Biodata Biodata = db.Biodatas
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (Biodata != null)
                    {
                        Biodata.is_deleted = true;
                        Biodata.deleted_by = "admin";
                        Biodata.deleted_on = DateTime.Now;
                        db.SaveChanges();
                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Data tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        public static BiodataViewModel ById(long id)
        {
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new CampContext())
            {
                result = (from c in db.Biodatas
                          where c.is_deleted == false && c.id == id
                          select new BiodataViewModel
                          {
                              id = c.id,
                              fullName = c.fullName
                          }).FirstOrDefault();
            }
            return result;
        }
        public static List<BiodataViewModel> User()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            using (var db = new CampContext())
            {
                result = (from c in db.Biodatas
                          where c.is_deleted == false && c.position == 1
                          select new BiodataViewModel
                          {
                              id = c.id,
                              fullName = c.fullName
                          }).ToList();
            }
            return result;
        }
    }
}
