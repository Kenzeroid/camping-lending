﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Camp.DataModel
{
    public class BaseTable
    {
        [Required, StringLength(50)]
        public string create_by { get; set; }
        public DateTime create_on { get; set; }
        
        [StringLength(50)]
        public String modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public String deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        public bool is_deleted { get; set; }
    }
}
