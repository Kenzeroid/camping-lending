﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Camp.DataModel
{
    [Table("x_product")]
    public class Product:BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        [Required]
        public long amount { get; set; }
        [Required]
        public string initial { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public long price { get; set; }
    }
}
