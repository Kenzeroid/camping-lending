﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Camp.DataModel
{
    [Table("x_biodata")]
    public class Biodata:BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        [Required]
        public string fullName { get; set; }
        public string pob { get; set; }
        public string email { get; set; }
        public DateTime dob { get; set; }
        public int position { get; set; }
        public virtual ICollection<ReceiptHead> ReceiptHeads { get; set; }
    }
}
