﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Camp.DataModel
{
    [Table("x_receipthead")]
    public class ReceiptHead:BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        public long biodataId { get; set; }
        public String Code { get; set; }
        [Required]
        public long total { get; set; }
        [Required]
        public DateTime borrowTime { get; set; }
        public bool status { get; set; }
        public string returnTime { get; set; }
        public virtual Biodata Biodata { get; set; }
        public virtual ICollection<ReceiptDetail> ReceiptDetails { get; set; }

    }
}
