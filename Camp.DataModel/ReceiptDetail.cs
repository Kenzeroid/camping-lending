﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Camp.DataModel
{
    [Table("x_receiptdetail")]
    public class ReceiptDetail:BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        public long receiptHeadId { get; set; }
        public long productId { get; set; }
        public int quantity { get; set; }
        public long price { get; set; }
        public virtual ReceiptHead ReceiptHead { get; set; }
        public virtual Product Product { get; set; }
    }
}
