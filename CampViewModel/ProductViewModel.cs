﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CampViewModel
{
    public class ProductViewModel
    {
        public long id { get; set; }
        [Required]
        public long amount { get; set; }
        [Required]
        public string initial { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public long price { get; set; }

    }
}
