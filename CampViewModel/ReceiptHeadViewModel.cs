﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace CampViewModel
{
    public class ReceiptHeadViewModel
    {
        public long id { get; set; }
        public long biodataId { get; set; }
        public DateTime create_on { get; set; }
        public String Code { get; set; }
        [Required]
        public long total { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd-MMMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime borrowTime { get; set; }
        public bool status { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMMM-yyyy}", ApplyFormatInEditMode = true)]
        public string returnTime { get; set; }
        public List<ReceiptDetailViewModel> ReceiptDetailViewModel { get; set; }
    }
    public class ReceiptDetailViewModel
    {
        public long id { get; set; }
        public long receiptHeadId { get; set; }
        public long productId { get; set; }
        public int quantity { get; set; }
        public long price { get; set; }
        public long amount { get { return quantity * price; } set { } }
    }
}
