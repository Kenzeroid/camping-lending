﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Camp.DataModel;
using CampRepo;
using CampViewModel;

namespace CampLending.Controllers
{
    public class LendingController : Controller
    {
        // GET: Lending
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ListAll()
        {
            List<Product> list = ProductRepo.All();
            list = list.Where(a => a.is_deleted == false).ToList();
            IPagedList<Product> lists = list.ToPagedList(1, 5);
            return PartialView("_List", lists);
        }
        public ActionResult List(string nama, int? page, int? pagesize)
        {
            int pageSize = 5;
            int pageList = 1;
            pageSize = pagesize.HasValue ? Convert.ToInt32(pagesize) : 5;
            pageList = page.HasValue ? Convert.ToInt32(page) : 1;
            List<Product> list = ProductRepo.All();
            if (!String.IsNullOrEmpty(nama))
            {
                list = list.Where(a => a.name.ToUpper().Contains(nama.ToUpper())).ToList();
            }
            if (list.Count == 0)
            {
                return PartialView("_NotFound");
            }
            else
            {
                IPagedList<Product> lists = list.ToPagedList(pageList, pageSize);
                return PartialView("_List", lists);
            }
        }
        public ActionResult SelectedProduct(long id)
        {
            ProductViewModel product = ProductRepo.ById(id);
            ReceiptDetail receiptDetail = new ReceiptDetail();
            ViewBag.Product = ProductRepo.All();
            receiptDetail.productId = product.id;
            receiptDetail.price = product.price;
            return PartialView("_SelectedProduct", receiptDetail);
        }

        [HttpPost]
        public ActionResult Payment(ReceiptHeadViewModel model)
        {
            ViewBag.Product = ProductRepo.All();
            ViewBag.Biodata = new SelectList(BiodataRepo.User(), "id", "fullName");
            return PartialView("_Payment", model);
        }
        [HttpPost]
        public ActionResult Pay(ReceiptHeadViewModel model)
        {
            RespondResult result = ReceiptRepo.Payment(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    reference = result.Entity
                }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReturnMenu()
        {
            return View("ReturnMenu");
        }
        public ActionResult ReturnList(int? page, int? pagesize)
        {
            int pageSize = 5;
            int pageList = 1;
            pageSize = pagesize.HasValue ? Convert.ToInt32(pagesize) : 5;
            pageList = page.HasValue ? Convert.ToInt32(page) : 1;
            ViewBag.Biodata = BiodataRepo.All();
            List<ReceiptHeadViewModel> list = ReceiptRepo.receiptHeads();
            list = list.OrderBy(a => a.status).ThenByDescending(b => b.borrowTime).ToList();
            IPagedList<ReceiptHeadViewModel> lists = list.ToPagedList(pageList, pageSize);
            return PartialView("_ReturnList", lists);
        }
        public ActionResult DetailList(long id)
        {
            ViewBag.Product = ProductRepo.All();
            return PartialView("_DetailList", ReceiptRepo.Details(id));
        }
        [HttpPost]
        public ActionResult Returned(long id, int diff)
        {
            RespondResult result = ReceiptRepo.Return(id, diff);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                }, JsonRequestBehavior.AllowGet);
        }
    }
}