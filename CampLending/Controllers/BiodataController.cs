﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CampRepo;
using Camp.DataModel;
using System.Web.Mvc;

namespace CampLending.Controllers
{
    public class BiodataController : Controller
    {
        // GET: Biodata
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            List<Biodata> list = BiodataRepo.All();
            list = list.Where(a => a.is_deleted == false).ToList();
            return PartialView("_List", list);
        }

        public ActionResult Create()
        {
            return PartialView("_Create", new Biodata());
        }
        [HttpPost]
        public ActionResult Create(Biodata product)
        {
            RespondResult result = BiodataRepo.Update(product);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", BiodataRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(Biodata model)
        {
            RespondResult result = BiodataRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}