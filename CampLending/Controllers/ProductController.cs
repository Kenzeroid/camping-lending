﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CampRepo;
using Camp.DataModel;
using System.Web.Mvc;

namespace CampLending.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            List<Product> list = ProductRepo.All();
            list = list.Where(a => a.is_deleted == false).ToList();
            return PartialView("_List", list);
        }
        
        public ActionResult Create()
        {
            return PartialView("_Create", new Product());
        }
        [HttpPost]
        public ActionResult Create(Product product)
        {
            RespondResult result = ProductRepo.Update(product);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", ProductRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(Product model)
        {
            RespondResult result = ProductRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(long id)
        {
            return PartialView("_Edit", ProductRepo.ById(id));
        }
        [HttpPost]
        public ActionResult Edit(Product model)
        {
            RespondResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}