﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CampRepo;
using Camp.DataModel;
using System.Web.Mvc;

namespace CampLending.Controllers
{
    public class ReceiptController : Controller
    {
        // GET: Receipt
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", ReceiptRepo.All());
        }

        public ActionResult Create()
        {
            return PartialView("_Create", new ReceiptHead());
        }
        [HttpPost]
        public ActionResult Create(ReceiptHead product)
        {
            RespondResult result = ReceiptRepo.Update(product);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}